# computer basics

I will be able to:
  * use `OS shortcuts` for productivity gains
  * master `browser shortcuts` - its our window to the world!
  * understand `markup` as a "language", know basic `markdown` for note taking

Homework:
  * create central projects folder
  * create a blog post in markdown, containng:
    * a foto
    * external links to other sites
    * internal links to paragraph
    * 1 ordered list
    * 2 unordered lists

----

OS:
  * essentials apps - file explore, editors, clipboard manager, task manager
  * file management - files, file types, file paths, folder structure
  * shortcuts:
    * mac - [1](http://www.iphonehacks.com/2018/04/top-mac-keyboard-shortcuts.html), [2](https://www.computerworld.com/article/3023544/24-keyboard-shortcuts-mac-users-need-to-know.html)

Browser:
  * ajax, get request
  * bookmarks, history, etc
  * shortcuts:
    * `ctrl + e / cltr + l` - focus url
    * `alt + left/right` - back/forward
    * `ctrl + t` - new tab
    * `ctrl + shift + n` - new private tab
    * `ctrl + w` - close tab
    * `ctrl + shift + t` - restore closed tabs
    * `ctrl + j` - downloads

Text manipulation shortcuts:
  * `shift + left/right/home/end` - selects
  * `shift + ctrl + left/right` - selects by word
  * `ctrl + a` - selects all


## markdown

Plain text markup language widly used in programming and science communities.

Learning material:

  * visual step by step [tutorial](https://www.markdowntutorial.com/)
  * one of many [cheat sheets](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
