# auste-domantas-project

All the material needed for Auste and Domantas to conquer the world


## the plan

Birds eye view of how we are going to achieve the knowledge needed easily score at job interview.

The basics:
  1. computer [basics](basics)
  2. brew + [terminal](terminal) must knows
  3. [git](git) introduction

The fundamentals:
  1. [sql](sql)
  2. the editor/ide - [vscode](vscode)
  3. [python](python)

***Final goal - finishing atleast 1 video courses or data science in python.***

Later self improvement:
  * reading a book
  * advance excel

## lessons

Every segment's only purpuse should be to allow us to advance to another segment without friction - 
which means understanding the `scope` of the problem and learning the `practical` stuff.

There are few key thins every step should cover:
  * whats do I need to know after this step
  * what material can help me to learn
  * what homework should I (be able to) do
