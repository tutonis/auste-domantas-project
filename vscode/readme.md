# visual studio code

One of the most popular code editors (not IDE) with great development momentum and huge community backing
resulting in variety of plugins and fast support.

Useful plugins:
  * python - `ext install ms-python.python`
  * intelicode - `ext install VisualStudioExptTeam.vscodeintellicode`
  * git lense - `ext install eamodio.gitlens`

Editors shortcuts can be found
[here](https://docs.microsoft.com/en-us/visualstudio/ide/default-keyboard-shortcuts-in-visual-studio?view=vs-2019) 
or 
[here](https://www.dofactory.com/reference/visual-studio-shortcuts)
or where ever your google search takes you.

Essential shortcuts:
  
  * ctrl-p and friends
  * back/forward
  * jump to 

  
